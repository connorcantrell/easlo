from django.contrib import admin
from .models import Category, Make, Unit, UnitInstance, Location

admin.site.register(Category)
admin.site.register(Make)
admin.site.register(Unit)
admin.site.register(UnitInstance)
admin.site.register(Location)
# Register your models here.
