from django.db import models
from django.urls import reverse # Used to generate URLs by reversing the URL patterns
import uuid

class Category(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique ID for this particular model')
    name = models.CharField(max_length=100, help_text='Name of category (i.e. Air Conditioning')

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "Categories"


class Make(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique ID for this particular model')
    name = models.CharField(max_length=100, help_text='Name of Manufacturer (i.e. Carrier')

    def __str__(self):
        return self.name
    # supplier = contact info
    

class Unit(models.Model):
    """Model representing a unit(but not a specific instance of a unit)."""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique ID for this particular model')
    make = models.ForeignKey('Make', on_delete=models.SET_NULL, null=True, help_text='Make of particular Unit')
    model = models.CharField(max_length=200, help_text='Model number from this particular make')
    category = models.ManyToManyField(Category, help_text='Category of particular Unit')
#     classification = models.ManyToManyField(Classification, help_text='Select a classification/type for this unit')
#     attributes = models.UUIDField(defualt=uuid.uuid8)
#     log = models.CharField(max_length=100, help_text='Document containing:jj')
#     support = models.ForeignKey('Support', null=True)
#     materials = models.UUIDField(default=uuid.uuid8)  # TODO: Determine how to represent a variety of different types of materials in a standardized way
    
    def __str__(self):
        """String for representing the Model object."""
        return f'{self.model} ({self.make})'
    
    def get_absolute_url(self):
        """Returns the url to access a detail record for this unit."""
        return reverse('unit-detail', args=[str(self.model)])


class UnitInstance(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique ID for this particular model')
    unit = models.ForeignKey('Unit', on_delete=models.SET_NULL, null=True)
    serial = models.CharField(max_length=200, help_text='Serial number from this particular model')
    label = models.CharField(max_length=20, null=True)
    location = models.ForeignKey('Location', on_delete=models.SET_NULL, null=True, help_text='Building location of unit')
    floor = models.CharField(max_length=15, null=True)
    materials = models.UUIDField(default=uuid.uuid4) 
    
    STATUS = (
        ('0', 'Active'),
        ('1', 'Needs Attention'),
        ('2', 'Inactive'),
    ) 

    status = models.CharField(
        max_length=1,
        choices=STATUS,
        blank=True,
        default='normal',
        help_text='Unit\'s status',
    )

    def __str__(self):
        """String for representing the Model object."""
        return f'{self.label}'
    
    def get_absolute_url(self):
        """Returns the url to access a detail record for this unit."""
        return reverse('unit', args=[str(self.label)])


class Location(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique ID')
    alias = models.CharField(max_length=50, null=True)
    address = models.CharField(max_length=150)  # TODO: Improve addresses
    
    def __str__(self):
        """String for representing the Model object."""
        return self.alias
    
    def get_absolute_url(self):
        """Returns the url to access a detail record for this unit."""
        return reverse('location-detail', args=[str(self.alias)])


class Directory(models.Model):
    '''
    NOTE: 
        - Is it a good idea to group all types of contacts under one table?
        - The current design doesn't distinguish internal and external communication
            - Should these be separated?
    '''
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text='Unique ID')
    
    DEPARTMENTS = (
        ('0', 'Purchasing'),
        ('1', 'Scheduling'),
        ('2', 'Support'),
        ('3', 'Acounting and Finance'),
        ('4', 'Research and Development'),
        ('5', 'Production'),
        ('6', 'Marketing')
    )

    department = models.CharField(
        max_length=1,
        choices=DEPARTMENTS,
        blank=True,
        default=None
    )

    first_name = models.CharField(max_length=26) 
    last_name = models.CharField(max_length=26)
    company = models.CharField(max_length=50)
    phone = models.CharField(max_length=12) 
    email = models.EmailField(max_length=254) 



